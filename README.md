# Gaussian
ガウスの消去法と掃き出し法の計算量の違いを確認する。

## Description
外部ファイル(.csv)の行列を読み込んで計算をする。  

$` A = \left(
  \begin{array}{cccc}
    a_{11} & a_{12} & \ldots & a_{1n} \\
    a_{21} & a_{22} & \ldots & a_{2n} \\
    \vdots & \vdots & \ddots & \vdots \\
    a_{n1} & a_{n2} & \ldots & a_{nn}
  \end{array}
\right)
\left(
  \begin{array}{cccc}
    x_{11} \\
    x_{21}\\
    \vdots \\
    x_{n1}
  \end{array}
\right
)
 =
 \left(
   \begin{array}{cccc}
     b_{11} \\
     b_{21}\\
     \vdots \\
     b_{n1}
   \end{array}
 \right)
`$  

としたときの$`x`$ベクトルを求める。  
掃き出し法で計算したときの計算時間を`jordan.dat`として出力した。  
また、消去法で計算したときの計算時間を`elimination.dat`として出力した。   

### 実行結果
計算時間のグラフが、  
![graph](Calcuration_times.png)  
として得られた。  
<br>
また、プログラムを実行した結果として  
```
-  Average calculation time  -
Jordan      : 2587.780000[ms]
Elimination : 1624.990000[ms]
```  
が得られたため、  
掃き出し法(Gauss Jordan)に比べてガウスの消去法(Gauss Elimination)の方が約1.6倍早いことがわかった。

## Usage
```
$ gcc gaussian.c
$ ./a.out [dimension] [Filename - A] [Filename - B]
```

dimension : 行列の次元(3×3行列ならば 3)<br>
Filename - A : a行列のファイル名.csv<br>
Filename - B : b行列のファイル名.csv<br>
