#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define TIMES 100

// File read
void inputData(double *data[], int dimension, char filenameA[256], char filenameB[256]);

double gaussJordan(double *data[], int dimension);

double gaussElimination(double *data[], int dimension);

int main(int argc, char const *argv[]) {
  double **data;
  int dimension, i;
  double jordan,elimination;
  double jordan_sum = 0,elimination_sum = 0;
  char filenameA[256];
  char filenameB[256];
  FILE *fp;

  /* コンパイル時の処理 */
  if(argc-1 != 3) {
    printf("Error.\n");
    printf("Usage: ./a.out [dimension] [Filename - A] [Filename - B]\n");
    exit(1);
  }

  fp = fopen("jordan.dat", "w");
  if (fp==NULL) {
    printf("can't open file.\n");
    exit(1);
  }

  dimension = atoi(argv[1]);
  strcpy(filenameA, argv[2]);
  strcpy(filenameB, argv[3]);

  // 配列の確保
  data = (double **)malloc(sizeof(double *)* dimension);
  for (i = 0; i < dimension; i++) {
    data[i] = malloc(sizeof(double)* (dimension+1));
  }

  // gauss jordan で100回計算する
  for(i=0; i < TIMES; i++){
    // make matrix
    inputData(data, dimension, filenameA, filenameB);
    // gauss jordan
    jordan = gaussJordan(data, dimension);
    // 計算時間の合計値を算出
    jordan_sum += jordan;
    fprintf(fp, "%f\n", jordan);
  }
  fclose(fp);

  fp = fopen("elimination.dat", "w");
  if (fp==NULL) {
    printf("can't open file.\n");
    exit(1);
  }

  // gauss elimination で100回計算する
  for(i=0; i<TIMES; i++){
    // make matrix
    inputData(data, dimension, filenameA, filenameB);

    // gauss elimination
    elimination = gaussElimination(data, dimension);
    elimination_sum += elimination;
    fprintf(fp, "%f\n", elimination);
  }

  fclose(fp);

  // 平均計算時間を表示
  printf("\n\n");
  printf("-  Average calculation time  -\n");
  printf("Jordan      : %f[ms]\n", jordan_sum/TIMES);
  printf("Elimination : %f[ms]\n", elimination_sum/TIMES);

  // free data[][]
  for (i = 0; i < dimension; i++) {
    free(data[i]);
  }
  free(data);

  return 0;
}

void inputData(double *data[], int dimension, char filenameA[256], char filenameB[256])
{
  FILE *fp;
  int x,y;

  // open A
  fp = fopen(filenameA, "r");
  if(fp==NULL) {
    printf("Error.\n");
    printf("Can't open file A.\n");
    exit(1);
  }

  for(y = 0; y < dimension; y++) {
    for (x = 0; x < dimension; x++) {
      fscanf(fp, "%lf,", &data[y][x]);
    }
  }
  fclose(fp);

  // open B
  fp = fopen(filenameB, "r");
  if(fp==NULL) {
    printf("Error.\n");
    printf("Can't open file B.\n");
    exit(1);
  }

  for (y = 0; y < dimension; y++) {
    fscanf(fp, "%lf", &data[y][dimension]);
  }
  fclose(fp);
}

double gaussJordan(double *data[], int dimension)
{
  clock_t start, end;
  int i, j, k;

  start = clock();

  for (k = 0; k < dimension; k++) {
    for (j = k + 1; j < dimension + 1; j++) {
      data[k][j] = data[k][j] / data[k][k];
      for (i = 0; i < dimension; i++) {
        if (i != k) {
          data[i][j] = data[i][j] - data[k][j] * data[i][k];
        }
      }
    }
  }
  end = clock();

  printf("Gauss Jordan      : %ld [ms]\n", end - start);

  return end - start;
}

double gaussElimination(double *data[], int dimension)
{
  clock_t start, end;
  int i, j, k;

  start = clock();

  for (k = 0; k < dimension; k++) {
    for (j = k + 1; j < dimension+ 1; j++) {
      data[k][j] = data[k][j] / data[k][k];
      for (i = k + 1; i < dimension; i++) {
        data[i][j] = data[i][j] - data[k][j] * data[i][k];
      }
    }
  }

  double ans[dimension];

  ans[dimension-1] = data[dimension-1][dimension];
  for (k = dimension - 2; k >= 0; k--) {
    ans[k] = data[k][dimension];

    for (j = k + 1; j < dimension; j++) {
      ans[k] = ans[k] - data[k][j] * ans[j];
    }
  }

  end = clock();

  printf("Gauss Elimination : %ld [ms]\n", end - start);

  return end - start;

}
